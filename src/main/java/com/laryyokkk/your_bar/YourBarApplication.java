package com.laryyokkk.your_bar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class YourBarApplication {

    public static void main(String[] args) {
        SpringApplication.run(YourBarApplication.class, args);
    }

}
